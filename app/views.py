from django.shortcuts import render
from django.core.files.storage import FileSystemStorage

from redis import Redis, RedisError

# Create your views here.

redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)


def home(request):
    try:
        visites = redis.incr("compteur")
    except RedisError:
        visites = "<i>Erreur de connection Redis, compteur desactive</i>"

    return render(request, "index.html", {"visites": visites})


def image_upload(request):
    if request.method == "POST" and request.FILES["image_file"]:
        image_file = request.FILES["image_file"]
        fs = FileSystemStorage()
        filename = fs.save(image_file.name, image_file)
        image_url = fs.url(filename)
        print(image_url)
        return render(request, "upload.html", {"image_url": image_url})
    return render(request, "upload.html")
